# 2D Space Simulator

Play around with gravity to create your own systems of celestial objects.
Made with Unity, but without the built-in physics: wrote my own kinematics and collision resolution.

![](Screenshot_1.png)
